# frozen_string_literal: true

require 'vips'

require 'site_compare/configuration'
require 'site_compare/version'
require 'site_compare/vacuum'
require 'byebug'

module SiteCompare
  class Error < StandardError
  end

  # https://jeffkreeftmeijer.com/ruby-compare-images/
  def self.diff_images(first, second, out_file)
    images = [
      ChunkyPNG::Image.from_file(first),
      ChunkyPNG::Image.from_file(second)
    ]

    diff = []

    # Sometimes, a rgb(1,1,1) pixel pops and we don't want to keep it in the diff summary
    threshold = 1

    images.first.height.times do |y|
      images.first.row(y).each_with_index do |pixel, x|
        pixel = rgb(
          r(pixel) + r(images.last[x, y]) - 2 * [r(pixel), r(images.last[x, y])].min,
          g(pixel) + g(images.last[x, y]) - 2 * [g(pixel), g(images.last[x, y])].min,
          b(pixel) + b(images.last[x, y]) - 2 * [b(pixel), b(images.last[x, y])].min
        )

        images.last[x, y] = pixel

        diff << pixel if r(pixel) > threshold && g(pixel) >threshold && b(pixel) > threshold
      end
    end

    difference = diff.length.to_f / images.first.pixels.length

    # puts "pixels (total): #{images.first.pixels.length}"
    # puts "pixels changed: #{diff.length}"
    # puts "difference (%): #{ difference * 100}%"

    if(difference > 0)
      images.last.save(out_file)
    end

    { percentage: difference, pixels: diff.length }
  end

  class Comparator
    def self.run
      @@total_time = Time.now
      @@config     = SiteCompare::Configuration.new

      FileUtils.mkdir_p @@config.output_dir unless File.directory? @@config.output_dir

      vacuum = Vacuum.new @@config
      vacuum.run

      compare_dir(@@config.output_dir)
    end

    private_class_method def self.compare_dir(directory)
      compare_time = Time.now
      nodes        = Dir.entries(directory)

      nodes.each do |node|
        next if ['.', '..'].include?(node)

        path = File.join(directory, node)

        if File.directory? path
          compare_dir path
        end
      end

      files = Dir[File.join(directory, '*.png')]

      if files.count > 0 && files.count != 2
        puts ">> Skipping #{directory}. Diff can be made on two files exactly."
        return
      end

      return if files.count == 0

      puts "==== Processing files in #{directory}"
      puts '  => Cropping files...'
      crop_images files[0], files[1]

      puts '  => Comparing files...'
      diff_time   = Time.now
      summary = SiteCompare.diff_images File.join(files[0]),
                              File.join(files[1]),
                              File.join(directory, 'diff.png')

      if summary[:percentage] == 0.0
        puts '     No difference, removing sources'
        FileUtils.rm files[0]
        FileUtils.rm files[1]
      else
        puts "     Differences: #{summary[:pixels]} pixels (#{(summary[:percentage] / 100).round(8)}%)"
      end

      puts "---- Done in #{Time.now - diff_time}s / #{Time.now - compare_time}  #{Time.now - @@total_time}s.\n\n"
    end

    private_class_method def self.determine_image_height(vips_image)
      # find the value of the bottom right pixel ... we will search for all pixels
      # significantly different from this
      background = vips_image.getpoint((vips_image.width - 1), (vips_image.height - 1))

      # we need to smooth the image, subtract the background from every pixel, take
      # the absolute value of the difference, then threshold
      mask = (vips_image.median - background).abs > 10

      # sum mask rows and columns, then search for the first non-zero sum in each
      # direction
      _columns, rows = mask.project

      first_column, _first_row = rows.flipver.profile
      bottom                   = rows.height - first_column.min

      bottom
    end

    private_class_method def self.resize_and_save(vips_image, height, path)
      vips_image = vips_image.crop 0, 0, vips_image.width, height
      vips_image.write_to_file path
    end

    private_class_method def self.crop_images(first_path, second_path)
      first_image  = Vips::Image.new_from_file first_path
      second_image = Vips::Image.new_from_file second_path

      first_image_height  = determine_image_height(first_image)
      second_image_height = determine_image_height(second_image)

      final_height = first_image_height > second_image_height ? first_image_height : second_image_height

      resize_and_save first_image, final_height, first_path
      resize_and_save second_image, final_height, second_path
    end
  end
end
