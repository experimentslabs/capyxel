# frozen_string_literal: true

require 'byebug'
require 'capybara'
require 'capybara-screenshot'
require 'capybara/dsl'
require 'chunky_png'
require 'rubygems'
require 'selenium-webdriver'
require 'uri'
require 'webdrivers'

include ChunkyPNG::Color

module SiteCompare
  class Vacuum
    include Capybara::DSL

    def initialize(config)
      @config = config

      configure_capybara
    end

    def run
      @config.widths.each do |capture_width|
        @width     = capture_width
        @url_stack = @config.url_stack.dup
        @done      = []

        puts "Capturing pages with #{@width} window width"
        Capybara.page.driver.browser.manage.window.resize_to @width, 15_000

        while @url_stack.length.positive?
          url = @url_stack.shift
          @done.push url

          process url
        end
      end
    end

    private

    # Will process given websites, starting at root. Only items under the given
    # URLs will be saved in subfolders.
    #
    # Examples:
    # "https://inkline.io/docs/"
    # -> Start at "https://inkline.io/", create subfolders for everything under
    #    "docs/"
    #
    # "https://inkline.io/"
    # -> Start at "https://inkline.io/", create subfolders for everything under
    #    "/"
    #
    # "https://inkline.io/docs/index.html"
    # -> Start at "https://inkline.io/", create subfolders for everything under
    #    "docs/index.html", which means: almost nothing
    def process(url)
      puts "[#{@done.length}/#{@url_stack.length}] Visiting #{url}"
      @host = URI.parse(url).host
      visit url
      screenshot url if in_path? url

      process_actions url

      add_links if @config.crawl
    end

    def process_actions(url)
      section = extract_section(url)

      actions = @config.actions[section]

      return unless actions

      ['all', @width].each do |size|
        next unless actions[size]

        puts "  > Performing actions for #{section}"

        actions[size].each_key do |name|
          selector      = actions[size][name]['selector']
          manipulations = actions[size][name]['actions']

          manipulations.each do |manipulation|
            puts "    - #{name}"
            begin
              find(selector).send manipulation
              sleep 1

              directory = File.join(@config.output_dir, @width.to_s, section, name, manipulation)
              FileUtils.mkdir_p directory

              screenshot_and_save_page(prefix: File.join(directory, URI.parse(url).host), html: false)
            rescue Capybara::Ambiguous => e
              puts "      <!> Unable to perform action #{manipulation} for #{name}. Check your selector."
              puts "          #{e.message}"
            end
          end
        end
      end
    end

    def configure_capybara
      Capybara.register_driver(:selenium) do |app|
        browser_options = {
          browser: :chrome,
          options: Selenium::WebDriver::Chrome::Options.new(args: %w[headless no-sandbox])
        }
        Capybara::Selenium::Driver.new(app, browser_options)
      end
      Capybara.run_server     = false
      Capybara.default_driver = :selenium

      Capybara::Screenshot.append_timestamp = false
    end

    def add_links
      find_all('a[href]').each do |element|
        href = element['href']
        host = URI.parse(href).host

        if host == @host && !already_processed?(href) && !already_stacked?(href)
          @url_stack.push(href)
        end
      end
    end

    def already_processed?(url)
      @done.include? url
    end

    def already_stacked?(url)
      @url_stack.include? url
    end

    def extract_section(url)
      string = url.dup
      @config.websites.each { |w| string.sub! w, '' }

      string.gsub('#', '--').gsub(%r{[:/]}, '-')
    end

    def in_path?(url)
      string = url.dup
      @config.websites.each { |w| string.sub! w, '' }

      url != string
    end

    def screenshot(url)
      section   = extract_section(url)
      save_path = File.join(@config.output_dir, @width.to_s, section)

      FileUtils.mkdir_p save_path unless File.directory? save_path
      screenshot_path = File.join(save_path, URI.parse(url).host)
      screenshot_and_save_page(prefix: screenshot_path, html: false)
    end
  end
end
