require 'byebug'

module SiteCompare
  class Configuration
    attr_reader :websites, :widths, :crawl, :url_stack, :actions, :output_dir

    def initialize
      @output_dir = 'screenshots'

      load_configuration_file
      complete_url_stack

      puts_summary
    end

    private

    def load_configuration_file
      if ENV['CONFIG']
        file = ENV['CONFIG']
        raise "File not found '#{file}'" unless File.exist? file

        config = YAML.load_file file
      end

      config ||= {}

      config['websites']    ||= []
      config['websites'][0] = ARGV[0] if ARGV[0]
      config['websites'][1] = ARGV[1] if ARGV[1]
      @websites             = config['websites']

      config['widths'] ||= [1920]
      config['widths'] = ENV['WIDTHS'].split(',') if ENV['WIDTHS']
      @widths          = config['widths']

      config['urls'] ||= []
      if config['urls'].length.zero?
        config['urls'] = [
          url_base(config['websites'][0]),
          url_base(config['websites'][1]),
        ]
      else
        full_list = []
        config['websites'].each do |site|
          config['urls'].each { |u| full_list.push "#{site}#{u}" }
        end
        config['urls'] = full_list
      end
      @url_stack = config['urls']

      config['actions'] ||= {}
      @actions          = config['actions']

      # Not crawling if more than one stacked urls or if specified in ENV
      crawl             = false if ENV['LIST'] || @url_stack.length > 2
      crawl             = (ENV['CRAWL'] || true) != 'false'
      @crawl            = crawl

      raise "Incorrect amount of websites provided (#{config['websites'].length}/2)" if config['websites'].length < 2
    end

    def complete_url_stack
      if ENV['LIST']
        file = ENV['LIST']
        raise "File not found '#{file}'" unless File.exist? file

        @url_stack = []
        File.readlines(file).each do |line|
          @url_stack.push "#{base}#{line.chomp}"
        end
      end

      @url_stack ||= [@crawl ? @base : url]
    end

    def url_base(url)
      uri = URI.parse(url)
      "#{url.sub uri.path, ''}/"
    end

    def puts_summary
      puts 'SiteCompare initialized with current configuration:'
      puts '==================================================='
      puts "Crawler      : #{@crawl}"
      puts "Websites     : #{@websites}"
      puts "Window sizes : #{@widths}"
      puts "Prepared URLs: #{@url_stack.length}"
    end
  end
end
