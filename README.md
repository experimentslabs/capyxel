# SiteCompare

> Helps in comparing two versions of the same site to check the changes at a pixel level.

It's a combination of tools written in Ruby and it:

- scrapes each website and save a screenshot of every page (1920x15000 px)
- crops every screenshot to their smallest height
- compare the two versions and save a diff as png:

![docs/assets/diff.png](docs/assets/diff.png)

How to read it:
- black: identical content
- shades: tones/color differences

If you see it, that's different.

It was written to check consistency when converting [Inkline](https://github.com/inkline/inkline) styles from Stylus to SCSS.

## Installation

For now, clone it and `bundle install`

You will need [vips]() development library to compile native extensions

## Usage

```shell
# this will scrape http://localhost:8080 and https://inkline.io, making diffs for
# pages under http://localhost:8080/docs/ and https://inkline.io/docs/
bundle exec exe/site_comparator http://localhost:8080/docs/ https://inkline.io/docs/
```

**Notes:** The script will process given websites, starting at **root**. Only items **under the given URLs** will be saved in subfolders.

Examples:

- `https://inkline.io/docs`
  - Start at `https://inkline.io/`, create subfolders for everything under `docs/`
- `https://inkline.io`
  - Start at `https://inkline.io/`, create subfolders for everything under `/`
- `https://inkline.io/docs/index.html`
  - Start at `https://inkline.io/`, create subfolders for everything under `docs/index.html`, which means: _almost nothing_

If you want to compare one page only:

```shell
SCRAPE=false bundle exec exe/site_comparator page1 page2
```

With a predefined list of URLS like urls.txt:
```text
components/navbar
components/card
```

```shell
LIST=urls.txt bundle exec exe/site_comparator page1 page2
```

Change the capture width:
```shell
WIDTH=600 bundle exec exe/site_comparator page1 page2
```

For now, screenshots are saved in a `screenshot` directory in the project's directory.
## License

This project is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the SiteCompare project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/experimentslabs/site_compare/blob/master/CODE_OF_CONDUCT.md).

## Special thanks

I used code by [Jeff Kreeftmeijer](https://jeffkreeftmeijer.com/) in his article "[Comparing images and creating image diffs in Ruby](https://jeffkreeftmeijer.com/ruby-compare-images/)"

## Next steps
If you don't know what to do but want to get involved, I'd like to:

- Create screenshots of elements with different states (`:hover`, `:active`, `:focus`) but that needs a lot of work :/
- Find a way to create screenshots of page chunks
- Have a nice lib structure, usable as a gem
- Use a configuration file to use this in projects in CI (check consistency between builds)
- Start a changelog (beginners, welcome !)
- Write tests
- Integrate with gitlab CI (at least for Rubocop)
